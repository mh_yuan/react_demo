import React, { Component } from 'react';
import TodoItem from './components/TodoItem';
import axios from 'axios';
import  { Input, Button, List } from  'antd';
import './components/transition_style.css';
import 'antd/dist/antd.css';
import Form from './components/Form';
import Addition from './components/AdditionComponent';

class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [
            ],
            inputValue: '',
            show: true
        }

        // bind(this)写在构造函数里，只会执行一次
        this.inputChangeHandle = this.inputChangeHandle.bind(this);
        this.addItemHandle = this.addItemHandle.bind(this);
        this.deleteItemHandle = this.deleteItemHandle.bind(this);
        this.renderItemList = this.renderItemList.bind(this);
    }
    render() {
        return (
            <div style={{margin: '10px 5px'}}>
                <label htmlFor='insertArea'>请输入: </label>
                <Input
                    size='small'
                    style={{width: '300px'}}
                    placeholder='small input'
                    id="insertArea"
                    type="text"
                    value={this.state.inputValue}
                    onChange={this.inputChangeHandle}
                />
                <Button type="primary" onClick={this.addItemHandle}>add</Button>
                <List
                    style={{marginTop: '30px', width: '350px'}}
                    size="small"
                    header={<div>Header</div>}
                    footer={<div>Footer</div>}
                    bordered
                    dataSource={this.state.list}
                    renderItem={item => (<List.Item>{item}</List.Item>)}
                >
                </List>
                <Form />
                <Addition />
            </div>
        )
    }
    //componentWillMount 和 componentDidMount生命周期只会执行一次， ajax请求一般房子componentDidMount生命周期里面
    componentDidMount() {
        /*axios.get('/api/todolist')
            .then((resp) => {
                console.log('get resp', resp);
                this.setState(() => ({
                    list: resp.data
                }))
            })
            .catch((err) => {
                console.log(err)
            })*/

        // this.getData('/api/todolist');
    }
    async getData(url) {
        let res = await axios.get(url);
        res = res.data;
        console.log('res', res);
        if(res && res.length) {
            this.setState(() => ({
                list: [...res]
            }), () => {
                console.log('setState end')
            })
        }
        console.log('1111')


    }
    renderItemList() {
        return this.state.list.map((item, index) => {
            return (
                <TodoItem key={index} deleteItem={this.deleteItemHandle} content={item} index={index} />
            )

        })
    }

    inputChangeHandle(e) {
        // 旧版写法
        //this.setState ({
        //    inputValue: e.target.value
        //})
        // 新版写法, 变成了异步，需要对value做一下保存
        const value = e.target.value;
        this.setState(() => ({
                inputValue: value
            })
        )
    }

    deleteItemHandle(index) {
        this.setState((preState) => {
            const list = [...preState.list];
            list.splice(index, 1);
            return {
                list
            }
        })
    }

    addItemHandle() {
        // 旧版写法
        /*
        this.setState({
            list: [...this.state.list, this.state.inputValue],
            inputValue: ''
        })
        */
        // 新版写法, 箭头函数可以传一个参数， 代表之前的值
        this.setState((preState) => ({
            list: [...preState.list, preState.inputValue],
            inputValue: ''
        }))

    }

}

export default TodoList;