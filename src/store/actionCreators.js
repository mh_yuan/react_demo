import { SUBMIT_ITEM } from './actionTypes';

export const submitAction = (state, action) => ({
    type: SUBMIT_ITEM,
    name: state.name
})