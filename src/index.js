import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from './TodoList';

// const { Provider } = React.createContext();
// <Provider store={store}>
//     Component
// </Provider>
ReactDOM.render(<TodoList />, document.getElementById('root'));
