import React, {Fragment, Component } from 'react';
import PropTypes from 'prop-types';

const Temp = (props) => {
    const { content } = props;
    if(content > 0 && content < 100) {
        return <p>{ ` 0 < ${content} < 100`}</p>
    }else if(content >= 100 && content < 1000) {
        return <p>{`${content}: 3`}</p>
    } else {
        return <li>{`${content}`}</li>
    }
}

Temp.propTypes = {
    content: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ])
}

export default class Addition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: ''
        }
        this.changeInputHandle = this.changeInputHandle.bind(this);
    }
    render() {
        const { inputValue } = this.state;
        return (
            <Fragment>
                <input 
                    value={inputValue} 
                    type="number" 
                    onChange={this.changeInputHandle} 
                    style={{display: 'block', width: '200px', margin: '20px auto'}} 
                />
                <Temp content={inputValue} />
                <Temp content={inputValue * inputValue} />
            </Fragment>
        )
    }
    changeInputHandle(event) {
        let value = event.target.value;
        value = value.replace(/\D/g, '');
        this.setState(() => ({
            inputValue: value 
        }))
    }
}