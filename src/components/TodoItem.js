import React, { Component } from 'react';
import PropTypes from 'prop-types';


class TodoItem extends Component {
    constructor(props) {
        super(props);
        this.deleteItemHandle = this.deleteItemHandle.bind(this);
    }
    //static defaultProps = {
    //    content: 'hello world'
    //}
    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.content !== this.props.content ) return true;
        else return false;
    }
    render() {
        const { content } = this.props;

        return (
            <li
                onClick={this.deleteItemHandle}
                dangerouslySetInnerHTML={{__html: content}}
            >
            </li>
        )
    }
    deleteItemHandle() {
        const { deleteItem, index } = this.props;
        deleteItem(index);
    }
}

// 默认值
TodoItem.defaultProps = {
    content: 'hello world'
}

// 类型检测
TodoItem.propTypes = {
    content: PropTypes.string,
    deleteItem: PropTypes.func,
    index: PropTypes.number
}



export default TodoItem;