import React, { Component, Fragment } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
//import './transition_style.css';

class CssTransition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true
        }
        this.toggleHandle = this.toggleHandle.bind(this);
    }

    render() {
        return (
            <Fragment>
                <CSSTransition
                    in={this.state.show}
                    timeout={1000}
                    classNames='fade'
                    unmounOnExit
                    onEntered={(el) => {el.style.color = 'red'}}
                    appear={true}
                >
                    <div
                        className={this.state.show ? 'show' : 'hide'}
                    >hello world</div>
                </CSSTransition>
                <button onClick={this.toggleHandle}>切换</button>

            </Fragment>
        )
    }

    toggleHandle() {
        let value = this.state.show;
        this.setState(() => ({
            show: !value
        }), () => {
            console.log(this.state.show)
        })

    }
}

export default CssTransition;