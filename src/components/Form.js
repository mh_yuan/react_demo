import React, { Fragment } from 'react';
// import store from '../store/store';
// import { SUBMIT_ITEM } from '../store/actionTypes.js'
// import { submitAction } from '../store/actionCreators'

const FormApp = (props) => {
    const { name, submitHandle, changeHandle } = props;
    return (
        <form onSubmit={submitHandle} >
            <label>
                name: 
                <input type='text' name={name} onChange={changeHandle} />
            </label>
            <input type="submit" value='Submit' />
        </form>
    )
}
class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'name',
            valur: 'hello',
            list: [
                'hello', 'dear', 'mhyuan'
            ]
        }
        this.submitHandle = this.submitHandle.bind(this);
        this.changeHandle = this.changeHandle.bind(this);
    }
    render() {
        return (
            <Fragment>
                <FormApp name={this.state.name}  submitHandle={this.submitHandle} changeHandle={this.changeHandle} />
                <select  value={this.state.value} style={{display: 'block', margin:'20px auto' }} > {
                    this.state.list.map((item, index) => {
                        return (
                           <option key={index} value={item}>{item}</option>
                        )
                    })
                    }
                </select>
            </Fragment>
        )
    }
    submitHandle = (event) => {
        let name = this.state.name;
        console.log('submit !!', name);
        event.preventDefault();
        // const action = {
        //     type: SUBMIT_ITEM,
        //     name: name
        // };
        // store.dispatch(action);
        this.setState(() => ({
            name: ''
        }))
    }
    changeHandle(event) {
        let value = event.target.value;
        
        this.setState(() => ({
            name: value
        }))
    }

}

export default Form;